import OpenAI from "openai";
import { OpenAIStream, StreamingTextResponse } from "ai";

export const runtime = "edge";

const openai = new OpenAI({
  apiKey: process.env.OPENAI_API_KEY!,
});

export async function POST(req: Request) {
  // Extract the `prompt` from the body of the request

  // const { prompt, messages } = await req.json();

  // console.log(prompt)

  const data = await req.json();
  const { prompt, shouldChatGpt4 } = data;

  // Request the OpenAI API for the response based on the prompt
  const response = await openai.chat.completions.create({
    // model: "gpt-3.5-turbo",
    model: "gpt-4-1106-preview",
    // model: "gpt-4-1106-preview",
    stream: true,
    // a precise prompt is important for the AI to reply with the correct tokens
    messages: [
      {
        role: "user",
        content: prompt,
      },
    ],
    max_tokens: 1500,
    temperature: 0, // you want absolute certainty for spell check
    top_p: 0.1,
    frequency_penalty: 1,
    presence_penalty: 1,
    // user: "12345678",
  });

  const stream = OpenAIStream(response);

  return new StreamingTextResponse(stream);
}
