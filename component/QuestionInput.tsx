import { useState, useEffect } from "react";
import TextField from "@mui/material/TextField";
import { useCompletion } from "ai/react";

const FurtherQuestionInput = () => {
    const [currentApiCallResult, setCurrentApiCallResult] = useState<any>(null);
    const {
        completion,
        complete,
        data,
    } = useCompletion({
        api: "/api/completion",
    });

    const [inputValue, setInputValue] = useState("");

    useEffect(() => {
        if (!completion) return;

        setCurrentApiCallResult(completion);

    }
        , [completion]);

    // get user login info

    const handleChange = (event: any) => {
        setInputValue(event.target.value);
    };

    const handleSubmit = () => {

        const prompt = `
    Please answer my question below.

    ${inputValue}
    `;

        // trigger completion ai api call.
        complete(prompt);

        // Clear input field after submission or handle as needed.
        setInputValue("");
        // dispatch(updateQuestionPagePrompt(""))
    };

    const handleKeyPress = (event: any) => {
        if (event.key === "Enter" && !event.shiftKey) {
            // Prevent default behavior of enter key which is inserting a new line
            event.preventDefault();

            // Call your submit function
            handleSubmit();
        } else if (event.key === "Enter" && event.shiftKey) {
            // Allow shift+enter to create a new line by not doing anything special here.
            // The text field will naturally go to next line.
        }
    };
    return (
        <div className="w-[400px]">

            <div className="w-[400px] mb-20">{currentApiCallResult}</div>

            <TextField
                variant="outlined"
                placeholder="Any relevant question? (Please provide full context.)"
                value={inputValue}
                fullWidth
                multiline
                color="primary"
                sx={{ width: "600px", marginTop: "40px" }}
                onKeyDown={handleKeyPress}
                onChange={handleChange}
            />
        </div>
    );
};

export default FurtherQuestionInput;
